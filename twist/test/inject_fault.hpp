#pragma once

#include <wheels/core/source_location.hpp>

namespace twist::test {

// Inject voluntary fault
void InjectFault(wheels::SourceLocation call_site = wheels::SourceLocation::Current());

}  // namespace twist::test
