#pragma once

#include <wheels/core/source_location.hpp>

#include <fmt/core.h>

namespace fmt {

template <>
struct formatter<wheels::SourceLocation> {
  template <typename ParseContext>
  constexpr auto parse(ParseContext& ctx) {
    return ctx.begin();
  }

  template <typename FmtContext>
  auto format(wheels::SourceLocation source_loc, FmtContext& ctx) const {
    return ::fmt::format_to(ctx.out(), "{}:{} [Line {}]", source_loc.File(), source_loc.Function(), source_loc.Line());
  }
};

}  // namespace fmt