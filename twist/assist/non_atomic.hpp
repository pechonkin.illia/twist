#pragma once

/*
 * Annotations for data race checks
 *
 * twist::assist::NonAtomic<T>
 *
 * Usage:
 *
 * twist::assist::NonAtomic<int> var;
 *
 * *var = 1;
 * int v = var;
 *
 * // Capture source location
 * var.Write(1);
 * int v = var.Read();
 *
 * twist::assist::NonAtomic<Widget> w;
 *
 * w->Foo();
 * w.WriteView()->Foo();  // Capture source location
 *
 */

#include <twist/rt/facade/assist/non_atomic.hpp>

namespace twist::assist {

using rt::fiber::user::assist::NonAtomic;

}  // namespace twist::assist
