#pragma once

#if defined(__TWIST_FIBERS__) || PROC_COUNT == 1 || (defined(__TWIST_FAULTY__) && defined(LINUX))

#define __TWIST_SINGLE_CORE__ 1

#else

#define __TWIST_SINGLE_CORE__ 0

#endif
