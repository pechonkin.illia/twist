#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/static/thread_local/var.hpp>
#include <twist/rt/fiber/user/static/visit.hpp>

#define TWISTED_STATIC_THREAD_LOCAL_VAR(T, name) \
  static twist::rt::fiber::user::StaticThreadLocalVar<T> name{#name}; \
  ___TWIST_VISIT_STATIC_VAR(name)

#else

#include <twist/rt/thread/static/thread_local/var.hpp>

#define TWISTED_STATIC_THREAD_LOCAL_VAR(T, name) static thread_local twist::rt::thread::StaticThreadLocalVar<T> name

#endif
