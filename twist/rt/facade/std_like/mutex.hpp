#pragma once

#if defined(__TWIST_FIBERS__)

#include <twist/rt/fiber/user/library/std_like/mutex.hpp>
#include <twist/rt/fiber/user/library/std_like/timed_mutex.hpp>

namespace twist::rt::facade::std_like {

using mutex = rt::fiber::user::library::std_like::Mutex;  // NOLINT
using timed_mutex = rt::fiber::user::library::std_like::TimedMutex;  // NOLINT

}  // namespace twist::rt::facade::std_like

#elif defined(__TWIST_FAULTY__)

#include <twist/rt/thread/fault/std_like/mutex.hpp>
#include <twist/rt/thread/fault/std_like/timed_mutex.hpp>

namespace twist::rt::facade::std_like {

using mutex = rt::thread::fault::FaultyMutex;  // NOLINT
using timed_mutex = rt::thread::fault::FaultyTimedMutex;  // NOLINT

}  // namespace twist::rt::facade::std_like

#else

#include <mutex>

namespace twist::rt::facade::std_like {

using ::std::mutex;
using ::std::timed_mutex;

}  // namespace twist::rt::facade::std_like

#endif
