#pragma once

#include <optional>

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

namespace heap {

struct AllocatorParams {
  std::optional<unsigned char> memset;
};

}  // namespace heap

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
