#pragma once

#include "segment_manager.hpp"

#include "statics_allocator.hpp"
#include "heap/allocator.hpp"
#include "stack_allocator.hpp"

#include <twist/rt/fiber/system/params.hpp>
#include <twist/rt/fiber/system/fiber/id.hpp>

namespace twist::rt::fiber {

namespace system {

namespace memory::isolated {

// Isolated user memory

class Memory {
 public:
  Memory(const Params& params);

  void Reset();
  void Burn();

  // Heap (user)

  void* Malloc(size_t size);
  void Free(void* ptr);

  bool Access(void* ptr, size_t size);

  // Stacks (system)

  void SetMinStackSize(size_t);
  Stack AllocateStack(FiberId /*hint*/);
  void FreeStack(Stack stack, FiberId /*hint*/);

  heap::AllocatorStat HeapStat() const {
    return heap_.Stat();
  }

  // Static variables (user)

  void* AllocateStatic(size_t size);
  void DeallocateStatics();

  //

  bool FixedMapping() const;

 private:
  heap::AllocContext HeapAllocContext();

 private:
  bool randomize_malloc_;

  SegmentManager segments_;

  StaticsAllocator statics_;

  // Heap grows up, stacks - down
  heap::Allocator heap_;
  StackAllocator stacks_;
};

}  // namespace memory::isolated

}  // namespace system

}  // namespace twist::rt::fiber
