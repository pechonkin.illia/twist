#pragma once

namespace twist::rt::fiber {

namespace system {

enum class WakeType {
  SleepTimer,
  FutexWake,
  FutexTimer,
  Spurious,
};

}  // namespace system

}  // namespace twist::rt::fiber
