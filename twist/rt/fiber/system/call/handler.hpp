#pragma once

#include "../fwd.hpp"

#include <function2/function2.hpp>

namespace twist::rt::fiber {

namespace system::call {

using Handler = fu2::function_view<bool(Fiber*)>;

}  // namespace system::call

}  // namespace twist::rt::fiber
