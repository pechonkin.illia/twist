#pragma once

#include <twist/rt/fiber/system/fwd.hpp>
#include <twist/rt/fiber/system/fiber/user.hpp>
#include <twist/rt/fiber/system/fiber/id.hpp>
#include <twist/rt/fiber/system/fiber/state.hpp>
#include <twist/rt/fiber/system/fiber/trampoline.hpp>
#include <twist/rt/fiber/system/futex/wait_queue.hpp>
#include <twist/rt/fiber/system/futex/waiter.hpp>
#include <twist/rt/fiber/system/timer/timer.hpp>
#include <twist/rt/fiber/system/call/status.hpp>
#include <twist/rt/fiber/system/sync/atomic.hpp>
#include <twist/rt/fiber/system/sync/fiber.hpp>
#include <twist/rt/fiber/system/memory.hpp>
#include <twist/rt/fiber/system/object_allocator.hpp>
#include <twist/rt/fiber/system/scheduler/fiber.hpp>

#include <twist/rt/fiber/user/static/thread_local/manager.hpp>

#include <sure/context.hpp>

#include <wheels/intrusive/list.hpp>

#include <optional>
#include <string_view>

namespace twist::rt::fiber {

namespace system {

struct SchedulerTag {};
struct AliveTag {};

struct Fiber
    : private sure::ITrampoline,
      public TypedSystemObject<Fiber>,
      public wheels::IntrusiveListNode<Fiber, SchedulerTag>,
      public wheels::IntrusiveListNode<Fiber, AliveTag> {
 public:
  Fiber() = default;

  void Reset(Simulator* _sim,
             IFiberUserState* _user,
             Stack _stack,
             FiberId _id) {

    simulator = _sim;
    user = _user;
    stack = std::move(_stack);
    id = _id;

    parent_id.reset();

    state = FiberState::Starting;
    main = false;

    preemptive = true;

    futex = nullptr;
    timer = nullptr;
    waiter = nullptr;

    action = nullptr;

    runs = 0;

    sync.Init();

    context.Setup(stack->MutView(), /*trampoline=*/this);
  }

  // sure::ITrampoline
  void Run() noexcept override {
    Trampoline(this);
  }

  Simulator* simulator;
  IFiberUserState* user;  // Object from userspace
  std::optional<Stack> stack;
  sure::ExecutionContext context;
  FiberState state;
  user::tls::Storage fls;
  FiberId id;
  std::optional<FiberId> parent_id;
  bool main = false;

  bool preemptive = true;

  sync::Action* action;
  uint64_t action_ret;

  // For debugging
  const WaiterContext* waiter = nullptr;

  // Futex* system calls
  sync::AtomicVar* futex = nullptr;
  Timer* timer = nullptr;

  // For system calls
  call::Status status = call::Status::Ok;

  size_t runs{0};

  // For deadlock report from user-space
  std::string_view report;

  scheduler::FiberContext sched;
  sync::FiberContext sync;
};

}  // namespace system

}  // namespace twist::rt::fiber
