#pragma once

namespace twist::rt::fiber {

namespace system {

enum class Status : int {
  Ok = 0,
  LibraryAssert,
  UnhandledException,
  Deadlock,
  MemoryLeak,
  MemoryDoubleFree,
  MemoryAccess,
  DataRace,
  UserAbort,
  Pruned,
};

}  // namespace system

}  // namespace twist::rt::fiber
