#include "manager.hpp"

#include <twist/ed/static/var.hpp>

#include <twist/rt/fiber/system/simulator.hpp>

namespace twist::rt::fiber {

namespace user {

namespace tls {

extern const uintptr_t kSlotUninitialized = 1;

TWISTED_STATIC_VAR(Manager, instance);

Manager& Manager::Instance() {
  return instance.Ref();
}

/*
 * class Tls {
 *  public:
 *   ~Tls() {
 *     for (size_t i = 0; i < kIndexSize; ++i) {
 *       if (index_[i] != nullptr) {
 *         delete index_[i];
 *       }
 *     }
 *   }
 *  private:
 *   Chunk chunk0_;
 *   Index index_;
 * };
 *
 *
 */

Storage& Manager::Tls() {
  return system::Simulator::Current()->UserTls();
}

}  // namespace tls

}  // namespace user

}  // namespace twist::rt::fiber
