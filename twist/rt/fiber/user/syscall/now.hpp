#pragma once

#include <twist/rt/fiber/system/time.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

system::Time::Instant Now();

}  // namespace user::syscall

}  // namespace twist::rt::fiber
