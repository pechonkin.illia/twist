#pragma once

#include <twist/rt/fiber/system/fiber/id.hpp>
#include <twist/rt/fiber/system/fiber/user.hpp>

namespace twist::rt::fiber {

namespace user::syscall {

system::FiberId Spawn(system::IFiberUserState*);
void Detach(system::FiberId);

}  // namespace user::syscall

}  // namespace twist::rt::fiber
