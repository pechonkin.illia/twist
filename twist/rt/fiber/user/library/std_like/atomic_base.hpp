#pragma once

#include <twist/rt/fiber/user/syscall/sync.hpp>
#include <twist/rt/fiber/user/syscall/futex.hpp>

#include <twist/rt/fiber/user/scheduler/preemption_guard.hpp>
#include <twist/rt/fiber/user/scheduler/spurious.hpp>

#include <wheels/core/compiler.hpp>
#include <wheels/core/source_location.hpp>

// std::memory_order
#include <atomic>
// std::memcmp
#include <cstring>
#include <type_traits>
// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

template <typename T,
          bool Int = std::is_integral_v<T> && !std::is_same_v<T, bool>>
class AtomicBase;

// AtomicBase for non-integral types

template <typename T>
class AtomicBase<T, false> {
 public:
  using value_type = T;  // NOLINT

  // https://eel.is/c++draft/atomics.types.operations#1

  [[deprecated(
      "Atomic default constructor is error-prone. Please consider explicit initialization")]] AtomicBase() noexcept(std::
                                                    is_nothrow_default_constructible_v<
                                                        T>) {
    static_assert(std::is_default_constructible<T>());
  }

  AtomicBase(T value, wheels::SourceLocation source_loc) {
    Init(value, source_loc);
  }

  // Non-copyable
  AtomicBase(const AtomicBase<T>&) = delete;
  AtomicBase<T>& operator=(const AtomicBase<T>&) = delete;

  // Non-movable
  AtomicBase(AtomicBase<T>&&) = delete;
  AtomicBase<T>& operator=(AtomicBase<T>&&) = delete;

  ~AtomicBase() {
    Destroy();
  }

  bool is_lock_free() const noexcept {
    return true;
  }

  static constexpr bool is_always_lock_free = true;

  // NOLINTNEXTLINE
  T load(std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) const {
    system::sync::Action load{(void*)this, system::sync::ActionType::AtomicLoad, 0, mo,
                              "atomic::load", call_site};
    uint64_t r = syscall::Sync(&load);

    return ToValue(r);
  }

  T DebugLoad(std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) const {
    scheduler::PreemptionGuard g;

    system::sync::Action load{(void*)this, system::sync::ActionType::AtomicDebugLoad, 0, mo,
                              "atomic::DebugLoad", call_site};
    uint64_t r = syscall::Sync(&load);

    return ToValue(r);  // Do not trace
  }

  operator T() const {
    return load();
  }

  // NOLINTNEXTLINE
  void store(T new_value, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    system::sync::Action store{this, system::sync::ActionType::AtomicStore, ToRepr(new_value), mo,
                               "atomic::store", call_site};
    syscall::Sync(&store);
  }

  // NOLINTNEXTLINE
  T exchange(T new_value, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    system::sync::Action load{this, system::sync::ActionType::AtomicRmwLoad, 0, mo,
                              "atomic::exchange", call_site};
    uint64_t r = syscall::Sync(&load);

    {
      scheduler::PreemptionGuard g;
      system::sync::Action store{this, system::sync::ActionType::AtomicRmwCommit, ToRepr(new_value), mo,
                                 "atomic::exchange", call_site};
      syscall::Sync(&store);
    }

    return ToValue(r);
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(
      T& expected, T desired,
      std::memory_order success = std::memory_order::seq_cst,
      std::memory_order failure = std::memory_order::seq_cst,
      wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(failure);  // TODO

    system::sync::Action load{this, system::sync::ActionType::AtomicRmwLoad, 0, success,
                              "atomic::compare_exchange_strong", call_site};
    uint64_t r = syscall::Sync(&load);

    T old_value = ToValue(r);

    if (BitwiseEqual(expected, old_value)) {
      scheduler::PreemptionGuard g;
      system::sync::Action store{this, system::sync::ActionType::AtomicRmwCommit, ToRepr(desired), success,
          "atomic::compare_exchange_strong", call_site};
      syscall::Sync(&store);
      return true;
    } else {
      expected = old_value;
      return false;
    }
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(
      T& expected, T desired,
      std::memory_order success = std::memory_order::seq_cst,
      std::memory_order failure = std::memory_order::seq_cst,
      wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(failure);  // TODO

    system::sync::Action load{this, system::sync::ActionType::AtomicRmwLoad, 0, success,
                              "atomic::compare_exchange_weak", call_site};
    uint64_t r = syscall::Sync(&load);

    T old_value = ToValue(r);

    if (BitwiseEqual(expected, old_value)) {
      if (scheduler::SpuriousTryFailure()) {
        return false;
      }

      scheduler::PreemptionGuard g;
      system::sync::Action store{this, system::sync::ActionType::AtomicRmwCommit, ToRepr(desired), success,
                                 "atomic::compare_exchange_weak", call_site};
      syscall::Sync(&store);
      return true;
    } else {
      expected = old_value;
      return false;
    }
  }

  // wait / notify

#if defined(__TWIST_ATOMIC_WAIT__)

  // NOLINTNEXTLINE
  void wait(T old, std::memory_order mo = std::memory_order::seq_cst, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    system::WaiterContext waiter{system::FutexType::Atomic, "atomic::wait", call_site};
    while (BitwiseEqual(load(mo), old)) {
      syscall::FutexWait(this, ToRepr(old), &waiter);
    }
  }

  // NOLINTNEXTLINE
  void notify_one(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(call_site);
    // TODO: Interrupt
    syscall::FutexWake(this, 1);
  }

  // NOLINTNEXTLINE
  void notify_all(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
    WHEELS_UNUSED(call_site);
    // TODO: Interrupt
    syscall::FutexWake(this, 0);
  }

#endif

 private:
  void Init(T value, wheels::SourceLocation source_loc) {
    {
      scheduler::PreemptionGuard g;  // ???

      system::sync::Action init{this, system::sync::ActionType::AtomicInit, ToRepr(value), std::memory_order::relaxed /* ignored */,
                                "atomic::atomic", source_loc};
      syscall::Sync(&init);
    }
    std::memset(buf_, 0, sizeof(T));
  }

  void Destroy() {
    scheduler::PreemptionGuard g;
    system::sync::Action destroy{this, system::sync::ActionType::AtomicDestroy, 0, std::memory_order::relaxed /* ignored */,
                                 "atomic::~atomic", wheels::SourceLocation::Current()};
    syscall::Sync(&destroy);
  }

 protected:
  static uint64_t ToRepr(T v) {
    uint64_t r = 0;
    std::memcpy(&r, &v, sizeof(T));
    return r;
  }

  static T ToValue(uint64_t r) {
    T v;
    std::memcpy(&v, &r, sizeof(T));
    return v;
  }

  static bool BitwiseEqual(const T& lhs, const T& rhs) {
    return std::memcmp(&lhs, &rhs, sizeof(T)) == 0;
  }

 private:
  alignas(T) char buf_[sizeof(T)];
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
