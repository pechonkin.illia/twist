#pragma once

#include "atomic.hpp"
#include "mutex_owner.hpp"

#include <twist/rt/fiber/user/syscall/futex.hpp>

// #include <wheels/core/assert.hpp>
#include <twist/rt/fiber/user/safety/assert.hpp>

// std::lock_guard, std::unique_lock
#include <mutex>
// std::exchange
#include <utility>

namespace twist::rt::fiber {

namespace user::library::std_like {

class Mutex {
  enum State : uint32_t {
    Unlocked = 0,
    Locked = 1,
  };

 public:
  Mutex(wheels::SourceLocation source_loc = wheels::SourceLocation::Current())
      : locked_(State::Unlocked, source_loc) {
  }

  // Non-copyable
  Mutex(const Mutex&) = delete;
  Mutex& operator=(const Mutex&) = delete;

  // Non-movable
  Mutex(Mutex&&) = delete;
  Mutex& operator=(Mutex&&) = delete;

  // std::mutex / Lockable

  void lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    while (locked_.exchange(State::Locked, std::memory_order::seq_cst, call_site) == State::Locked) {
      system::WaiterContext waiter{system::FutexType::MutexLock, "mutex::lock", call_site};
      syscall::FutexWait(&locked_, State::Locked, &waiter);
    }
    owner_.Lock();
  }

  bool try_lock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    uint32_t unlocked = State::Unlocked;
    if (locked_.compare_exchange_weak(unlocked, State::Locked, std::memory_order::seq_cst, std::memory_order::relaxed, call_site)) {
      owner_.Lock();
      return true;
    } else {
      return false;
    }
  }

  void unlock(wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {  // NOLINT
    owner_.Unlock();
    locked_.store(State::Unlocked, std::memory_order::seq_cst, call_site);
    syscall::FutexWake(&locked_, 1);
  }

 private:
  Atomic<uint32_t> locked_;
  MutexOwner owner_;
};

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
