#pragma once

#include <twist/rt/fiber/user/syscall/sync.hpp>

#include <wheels/core/source_location.hpp>

// std::memory_order
#include <atomic>

namespace twist::rt::fiber {

namespace user::library::std_like {

// NOLINTNEXTLINE
inline void atomic_thread_fence(std::memory_order mo, wheels::SourceLocation call_site = wheels::SourceLocation::Current()) {
  system::sync::Action fence{nullptr, system::sync::ActionType::AtomicThreadFence, 0 /*ignored*/, mo, "atomic_thread_fence", call_site};
  syscall::Sync(&fence);
}

}  // namespace user::library::std_like

}  // namespace twist::rt::fiber
