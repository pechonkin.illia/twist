#pragma once

namespace twist::rt::thread::fault {

enum FaultPlacement {
  Before,
  After,
  Some,
};

}  // namespace twist::rt::thread::fault
