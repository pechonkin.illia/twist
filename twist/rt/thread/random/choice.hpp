#pragma once

#include "number.hpp"

#include <wheels/core/assert.hpp>

#include <cstdlib>

namespace twist::rt::thread {

namespace random {

// [0, alts)
inline size_t Choose(size_t alts) {
  WHEELS_ASSERT(alts > 0, "Positive number of alternatives expected");
  return Number() % alts;
}

// ~ Choice(2)
inline bool Either() {
  return Choose(2) == 0;
}

}  // namespace random

}  // namespace twist::rt::thread
