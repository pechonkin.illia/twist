#pragma once

#include <cstdint>

namespace twist::rt::thread {

namespace futex {

struct WakeKey {
  uint32_t* addr;
};

}  // namespace futex

}  // namespace twist::rt::thread
