#pragma once

#include <twist/rt/fiber/system/params.hpp>
#include <twist/rt/fiber/scheduler/random/params.hpp>

namespace twist::run::sim {

struct Params {
  rt::fiber::system::Params sim;
  rt::fiber::system::scheduler::random::Params sched;
};

}  // namespace twist::run::sim
