#pragma once

#include <twist/rt/fiber/scheduler/dfs/scheduler.hpp>

namespace twist::run::sim {

namespace dfs {

using Scheduler = rt::fiber::system::scheduler::dfs::Scheduler;

using Params = Scheduler::Params;
using Schedule = Scheduler::Schedule;

}  // namespace dfs

}  // namespace twist::run::sim
