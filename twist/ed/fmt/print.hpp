#pragma once

/*
 * Drop-in replacement for ::fmt::print and ::fmt::println
 *
 * void fmt::Print(format, args...)
 * void fmt::Println(format, args...)
 *
 * https://fmt.dev/latest/index.html
 *
 * Usage:
 *
 * twist::run::Cross([] {
 *   twist::ed::fmt::Println("Hello, {}!", "world");
 * });
 *
 */

#include <twist/rt/facade/fmt/print.hpp>

#include <fmt/core.h>

namespace twist::ed {

namespace fmt {

using rt::facade::fmt::Print;
using rt::facade::fmt::Println;

}  // namespace fmt

}  // namespace twist::ed
