#pragma once

/*
 * [[noreturn]] void system::Exit0();
 *
 * Simulation-only
 * Terminates simulation with Status::Ok
 *
 * Usage:
 *
 * auto ret = twist::run::Sim([] {
 *   twist::ed::system::Exit0();
 *
 *   WHEELS_UNREACHABLE();
 * });
 *
 * assert(ret.Ok());
 *
 */

#include <twist/rt/facade/system/exit0.hpp>

namespace twist::ed {

namespace system {

// [[noreturn]]
using rt::facade::system::Exit0;

}  // namespace system

}  // namespace twist::ed
